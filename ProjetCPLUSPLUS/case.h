#ifndef CASE_H
#define CASE_H
#include "EnumPiece.h"

class Case{

private:

    int numLine;
    int numColumn;
    int playerOnCase;
    EnumPiece pieceType;



public:

    Case();

    Case(int _numLine, int _numColumn);

    Case(int _numLine, int _numColumn, int _playerOnCase,const EnumPiece& _pieceType);

    ~Case();

    bool CaseIsEmpty();

    int GetNumLine();

    int GetNumColumn();

    int GetPlayerOnCase();

    inline EnumPiece& GetPieceType();

};

#endif // CASE_H
