#ifndef CASEVECTOR_H
#define CASEVECTOR_H

#include <vector>
#include "case.h"

class CaseVector
{
public:
        CaseVector();
        const std::vector<Case> GetCases();
        int GetLength();
        bool IsFull();
        bool IsEmpty();
protected:
    std::vector<Case> cases;
};

#endif // CASEVECTOR_H
