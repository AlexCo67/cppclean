#include "casevector.h"

CaseVector::CaseVector()
{

}

const std::vector<Case> CaseVector::GetCases(){
    return this->cases;
}

int CaseVector::GetLength(){
    return static_cast<int>(this->cases.size());
}

bool CaseVector::IsFull(){
    for(Case caseToCheck : this->cases){
        if(caseToCheck.CaseIsEmpty()){
            return false;
        }
    }
    return true;
}

bool CaseVector::IsEmpty(){
    for(Case caseToCheck : this->cases){
        if(!caseToCheck.CaseIsEmpty()){
            return false;
        }
    }
    return true;
}
