#include "case.h"

Case::Case()
{
}

Case::Case(int _numLine, int _numColumn): numLine(_numLine), numColumn(_numColumn), playerOnCase(0), pieceType(EnumPiece::EMPTY)
{
}

Case::Case(int _numLine, int _numColumn, int _value, const EnumPiece& _pieceType): numLine(_numLine), numColumn(_numColumn), playerOnCase(_value), pieceType(_pieceType)
{
}

bool Case::CaseIsEmpty()
{
    return(this->playerOnCase==0);
}
