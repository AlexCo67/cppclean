#ifndef ENUMPIECE_H
#define ENUMPIECE_H

enum class EnumPiece : int  {
    EMPTY = 0,
    PAWN = 1,
    QUEEN = 2,
};

#endif // ENUMPIECE_H

