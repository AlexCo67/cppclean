TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        case.cpp \
        casevector.cpp \
        main.cpp \
        player.cpp

HEADERS += \
    EnumPiece.h \
    case.h \
    casevector.h \
    player.h
